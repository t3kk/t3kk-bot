import {Client, VoiceConnection, VoiceChannel} from 'discord.js';
import { PlaybackQueue } from './bot/PlaybackQueue';
import express, { response } from 'express';
import axios from 'axios';
import FormData from 'form-data';

import config from '../config.json';


const client = new Client();

client.on('ready', () => {
  console.log(`Logged in as ${client.user?.username}!`);
});

client.on('message', msg => {
  if (msg.content === 'ping') {
    msg.reply('pong');
  }
  if (msg.content === '@joinme') {
    //Move bot to user's voice channel
  }
  if (msg.content && msg.content.startsWith('@play')) {
    if (msg.guild!=null){
      let playbackQueue = PlaybackQueue.getPlaybackQueue(msg.guild);
      playbackQueue.handleMusicRequestMessage(msg);
    } else {
      msg.reply("guild was null");
      console.log(`guild was null for message [${msg}]`);
    }
  }
});


client.login(config.discordToken);

const app = express();
const port = 9000;

app.use(express.json()) // for parsing application/json

//Setup CORS
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://localhost:3000"); // update to match the domain you will make the request from
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


app.get('/', (req, res) => {
  let guild = client.guilds.cache.get('314260605177692163');
  let playbackQueue:PlaybackQueue;
  if (guild) {
    playbackQueue = PlaybackQueue.getPlaybackQueue(guild);
    res.send(playbackQueue);
  } else {
    res.send("ERROR")
  }
});

app.post('/auth', (req, res) => {
  console.log(req.body);
  let code: string = req.body?.code;
  if (code === undefined || code === null) {
    res.statusCode = 400;
    res.send();
  } else {
    
    const data = new URLSearchParams();
    data.append('client_id', config.clientId);
    data.append('client_secret', config.clientSecret);
    data.append('grant_type', 'authorization_code');
    data.append('code', code);
    data.append('redirect_uri', 'http://localhost:3000/auth');
    data.append('scope', 'identify guilds');


    axios.post('https://discordapp.com/api/oauth2/token', data, { headers: { "Content-Type": "application/x-www-form-urlencoded" } }).then(response => {
      //take the accesstoken and put into cookie
      //store refresh token so that later we don't need to go to discord for login
      res.send(response.data);
    }).catch(error => {
      console.error(error);
    })
  }
})

app.get('/t3kk-bot/', (req, res) => {
  let accessCode = req.query.code;
  //todo: abstact out - see if we really weantch fetch and Formdata
  if (accessCode) {
    // let client = http2.connect('https://discordapp.com')
  }
})


app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`));