import { Snowflake, VoiceConnection, Guild, VoiceChannel, Collection } from "discord.js";


export class VoiceManagement {
    private static voiceConnections = new Map<Snowflake, VoiceConnection>();
    private static voiceChannels = new Map<Snowflake, VoiceChannel>();

    static getVoiceConnectionForGuild(guild: Guild){
        //connection by guild or connection by channel and then guild?  
        // sounds like only one connection allowed per guild.....
        let activeChannel = this.getActiveChannelForGuild(guild);
        if (activeChannel == null){
            //throw an error?
            console.error("couldn't find a good voice channel");
        } else {
            
        }
    }


    private static setActiveChannelForGuild(guild: Guild, voiceChannel: VoiceChannel) {
        VoiceManagement.voiceChannels.set(guild.id, voiceChannel);
    }
    

    private static getActiveChannelForGuild(guild: Guild): VoiceChannel | null {
        if (!VoiceManagement.voiceChannels.has(guild.id)) {
            return VoiceManagement.voiceChannels.get(guild.id) as VoiceChannel;
        } else {
            return this.findDefaultActiveChannelForGuild(guild);
        }

    }

    //If there is no active channel, use the first one that is pn the list with a user
    private static findDefaultActiveChannelForGuild(guild: Guild): VoiceChannel | null{
        //Filter out all non voice channels
        let activeVoiceChans = guild.channels.cache.filter(channel => {
            return (channel.type === 'voice' && (channel as VoiceChannel).members.size>0);
        }) as Collection<string, VoiceChannel>;

        activeVoiceChans.sort((a,b)=>{
            return a.position-b.position;
        })

        //If there are no active voice channels, we don't wanna make a new one.
        if (activeVoiceChans.first()) {
            let activeChannel = activeVoiceChans.first()
            if (activeChannel!=undefined) {
                this.setActiveChannelForGuild(guild, activeChannel);
                return activeChannel;
            }
        }
        return null;
    }
}
