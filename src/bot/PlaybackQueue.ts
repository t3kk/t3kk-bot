import { Guild, VoiceChannel, VoiceConnection, GuildChannel, Message, StreamDispatcher } from "discord.js";
import ytdl from 'ytdl-core';
import ytpl from 'ytpl';

const playRegex : RegExp = /@play (?<url>\S*$)/;

export class PlaybackQueue{
    private static queuesByGuild = new Map<string, PlaybackQueue>();
    private voiceChannel: VoiceChannel;
    private voiceConnection: VoiceConnection;
    private requests = new Array<VideoInfo>();


    static getPlaybackQueue(guild: Guild){
        let playbackQueue = PlaybackQueue.queuesByGuild.get(guild.id);
        if (playbackQueue == undefined){
            playbackQueue = new PlaybackQueue(guild)
            PlaybackQueue.queuesByGuild.set(guild.id, playbackQueue);
        }
        return playbackQueue;
    }

    private constructor(guild: Guild){
        // VoiceManagement.getVoiceConnectionForGuild(guild);
        this.voiceChannel = guild.channels.cache.get('314260605177692163') as VoiceChannel;
    }

    private addMusicRequestToQueue(music: VideoInfo): number {
        this.requests.push(music);
        return this.requests.length;
    }

    //TODO: Save videos to files via workers, check for file upon a request.
    //create own naming scheme, use that to manage in progress requests.
    async handleMusicRequestMessage(message: Message){
        let {groups:{url}} = playRegex.exec(message.content) as any;
        //check for playlist
        try {
            let playlistID = await ytpl.getPlaylistID(url);
            let playlist = await ytpl(playlistID)
            playlist.items.forEach((item, index) => {
                console.log(index, item)
            })
            let videoOrder = new Array<Promise<VideoInfo>>();
            for (const item of playlist.items) {
                videoOrder.push(getVideoInfo(item.url));
            }

            let videoInfos:Array<VideoInfo> = await Promise.all(videoOrder);
            videoInfos.forEach((video) => {
                this.addMusicRequestToQueue(video);
            })
        } catch {
            let videoInfo = await getVideoInfo(url);
            let position = this.addMusicRequestToQueue(videoInfo);
            //TODO: use rich embed
            message.reply(`${videoInfo.title} added to queue in position ${position}`)
            if (message.deletable) {message.delete};
        }
        //If nothign is playing, start something
        //TODO: update start playback to not have arace to queue songs
        this.startPlayback();
    }


    async processAsPlaylist(url: string) {
        // ytpl.validateURL
    }


    async startPlayback(){
        console.log('HELLO')
        //If nothing is playing
        if (!this.voiceConnection || !this.voiceConnection.dispatcher || this.voiceConnection.dispatcher.destroyed) {
            //And a song is ready in the queue
            console.log('HELLO2')
            if(!this.isQueueEmpty()){
                console.log('HELLO3')
                //If the connection doesn't exist, make it
                if (!this.voiceConnection){
                    this.voiceConnection = await this.voiceChannel.join();
                    console.log('HELLO4')
                }
                this.playNextSong();
            }
        }
    }

    async playNextSong(){
        const streamOptions = { seek: 0, volume: .2 };
        //StartPlaying
        let nextStream = this.getNextStream();
        if (nextStream!= undefined) {
            let dispatcher = this.voiceConnection.play(nextStream, streamOptions);
            dispatcher.on("finish", () =>{
                this.playNextSong();
            });
        }
    }

    isQueueEmpty(){
        return this.requests.length==0;
    }

    getNextStream(){
        let next = this.requests.shift();
        if (next!=undefined){
            return ytdl(next.pageUrl, { filter : 'audioonly' });
        } else {
            return undefined;
        }
        
    }

}


export async function getVideoInfo(url:string): Promise<VideoInfo>{
    //
    let info = await ytdl.getInfo(url);
    return new VideoInfo(info.video_id, info.title, info.thumbnail_url, info.description, url);
  }  
  
  
  export class VideoInfo{
    public readonly id: string;
    public readonly title: string;
    public readonly thumbnail: string;
    public readonly description: string;
    public readonly pageUrl: string;
  
    constructor(id: string, title: string, thumbnail: string, description: string, pageUrl:string) {
      this.id = id;
      this.title = title;
      this.thumbnail = thumbnail;
      this.description = description;
      this.pageUrl = pageUrl;
   
    }
  }