FROM node:latest AS t3kk-bot
WORKDIR /build
RUN apt-get update && apt-get install -yy ffmpeg
COPY  package.json package-lock.json ./
RUN npm ci
COPY tsconfig.json config.json ./
COPY src/* src/
RUN ls src
RUN npm run build
CMD ["node", "./dist/src/index.js"]